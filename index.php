<?php
require "vendor/autoload.php"; 

// permet de charge twig
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/');
$twigConfig = array(
    'debug' => true,
);
// on dit à flight d'utiliser twig
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});




Flight::route('/', function(){
    
    $data = [
        "dinos" => tous_les_dinos(),
    ];
    Flight::view()->display('views/liste_dino.twig', $data);
});




Flight::route('/dinosaur/@slug', function($slug){
    $data = [
        "dino" => un_seul_dino($slug),
        "trois_dinos" => trois_dinos(),
    ];
    Flight::view()->display('views/dino_unique.twig', $data);
});


Flight::start();


